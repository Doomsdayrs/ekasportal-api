val jvm_toolchain: String by project
val coroutines_version: String by project

plugins {
	kotlin("multiplatform") version "1.8.10"
	idea
}

allprojects {
	apply(plugin = "org.gradle.maven-publish")

	group = "com.gitlab.doomsdayrs.lib"
	version = "0.1.0"

	repositories {
		mavenCentral()
		mavenLocal()
	}
}

idea {
}

kotlin {
	explicitApi()
	jvm("jvm") {
		jvmToolchain(jvm_toolchain.toInt())
		withJava()
		testRuns["test"].executionTask.configure {
			useJUnitPlatform()
		}
	}

	js(IR) {
		browser {
			commonWebpackConfig {
			}
		}
		binaries.library()
	}
	val hostOs = System.getProperty("os.name")
	val isMingwX64 = hostOs.startsWith("Windows")
	val nativeTarget = when {
		hostOs == "Mac OS X" -> macosX64("native")
		hostOs == "Linux" -> linuxX64("native")
		isMingwX64 -> mingwX64("native")
		else -> throw GradleException("Host OS is not supported in Kotlin/Native.")
	}

	fun kotlinx(module: String, version: String) =
		"org.jetbrains.kotlinx:$module:$version"

	sourceSets {
		val commonMain by getting {
			dependencies {
				api(kotlinx("kotlinx-coroutines-core", coroutines_version))
				api(kotlinx("kotlinx-datetime", "0.4.0"))
			}
		}
		val commonTest by getting
		val jvmMain by getting
		val jvmTest by getting
		val jsMain by getting
		val jsTest by getting
		val nativeMain by getting
		val nativeTest by getting
	}
}
