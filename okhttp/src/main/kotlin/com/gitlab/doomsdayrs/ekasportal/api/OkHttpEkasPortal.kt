/*
 *  Copyright (c) 2023 Doomsdayrs
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gitlab.doomsdayrs.ekasportal.api

import com.gitlab.doomsdayrs.ekasportal.data.*
import com.gitlab.doomsdayrs.ekasportal.exceptions.HTTPCodeException
import kotlinx.datetime.LocalDate
import kotlinx.datetime.LocalDateTime
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.executeAsync
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import java.util.*

/**
 * okhttp3 & jsoup implementation of [EkasPortal]
 *
 * @param client Client to use, should be configured with user agent.
 */
public class OkHttpEkasPortal(
	private val client: OkHttpClient
) : EkasPortal {
	public companion object {
		private val aryion = HttpUrl.Builder().scheme("https").host("aryion.com").build()
		private val g4 = aryion.newBuilder("g4/")!!.build()
		private val g4View = aryion.newBuilder("g4/view/")!!.build()
		private val g4Browse = g4.newBuilder("browse.php")!!.build()
		private val g4Search = g4.newBuilder("search.php")!!.build()
		private val g4Tags = g4.newBuilder("tags.php")!!.build()
		private val g4TagsQueue = g4.newBuilder("tagqueue.php")!!.build()
		private val g4Latest = g4.newBuilder("latest.php")!!.build()
	}

	private fun Element.getChildViews(): List<SimpleItem> = select("li").map { li ->
		val id = li.id().toInt()
		SimpleItem(
			id, g4.newBuilder(li.selectFirst("img")!!.attr("src"))!!.build().toString(),
			li.selectFirst("p.item-title")!!.text()
		)
	}

	private fun Document.getChildViews(): List<SimpleItem> =
		selectFirst(".gallery-items")!!.getChildViews()

	private fun Element.getLastPage(): Int =
		selectFirst("span.pagejumps")!!.select("a").last()?.text()?.toInt() ?: 1

	private fun createComment(comment: Element): CommentBuilder {
		val image = comment.selectFirst("img.avatar")!!
		val author = image.attr("alt")
		val authorImageURL = image.attr("src")

		return CommentBuilder(
			author,
			comment.selectFirst("a.user-link")!!.attr("href"),
			authorImageURL,
			comment.selectFirst("span.pretty-date")!!.attr("title"),
			comment.selectFirst("p.comment-body")!!.text(),
			// Comments on aryion are 'sub' commented by adding a margin of 36+ x depth
			comment.attr("style").substringAfter("margin-left: ").replace("px;", "").toInt() / 36
		)
	}

	/**
	 * @param depth If greater than 1, is likely a sub comment.
	 */
	private data class CommentBuilder(
		var author: String = "",
		var authorURL: String = "",
		var authorImageURL: String = "",

		var timestamp: String = "",

		var description: String = "",
		var depth: Int,
		val comments: ArrayList<CommentBuilder> = ArrayList()
	)

	private fun buildCommentBuilders(commentBuilder: List<CommentBuilder>): List<Comment> =
		commentBuilder.map { (author, authorURL, authorImageURL, timestamp, description, _, comments) ->
			Comment(
				author,
				authorURL,
				authorImageURL,
				timestamp,
				description,
				buildCommentBuilders(comments)
			)
		}

	@Throws(HTTPCodeException::class)
	override suspend fun getGalleryBrowse(sortType: SortType, page: Int): BrowsePage {
		val url = g4Browse.newBuilder()
			.addQueryParameter("sort", sortType.url)
			.addQueryParameter("p", "$page")
			.build()

		val response = client.newCall(Request.Builder().url(url).build()).executeAsync()

		if (response.isSuccessful) {
			response.body!!.use { responseBody ->
				val document = Jsoup.parse(responseBody.byteStream(), null, url.toString())

				val views = document.getChildViews()

				val lastPage = document.getLastPage()

				return BrowsePage(
					sortType, page, lastPage, views
				)
			}
		} else throw HTTPCodeException(response.code)
	}

	@Throws(HTTPCodeException::class)
	override suspend fun getGalleryView(id: Int): View {
		val url = g4View.newBuilder(View.createViewURL(id))!!.build()
		println(url)
		val response = client.newCall(Request.Builder().url(url).build()).executeAsync()


		if (!response.isSuccessful) throw HTTPCodeException(response.code)

		return response.body!!.use { responseBody ->
			val document = Jsoup.parse(responseBody.byteStream(), null, url.toString())

			val title = document.selectFirst(".g-box-title")!!.ownText().replaceFirst("<", "")
				.replaceFirst(">", "").trim()

			val gallery = document.selectFirst(".gallery-items")
			val authorName: String =
				document.selectFirst("div.ui-corner-top")!!.select("li a").last()!!.text()

			if (gallery != null) {
				val lastPage = document.getLastPage()
				View.FolderView(
					id, title, 1, lastPage, gallery.getChildViews()
				)
			} else {
				val header = document.selectFirst("div.g-box-header")!!

				val prev: String? = header.selectFirst("a#prev-link")?.attr("href")
				val next: String? = header.selectFirst("a#next-link")?.attr("href")

				val info = document.selectFirst("div.g-box div.g-box-contents")!!
				val authorImageURL: String = document.selectFirst("img.avatar")!!.attr("src")
				val description: String = info.selectFirst("p")!!.text()

				val detailBox = info.selectFirst("div.item-detail")!!
				var uploaded = ""
				var views = 0
				var fileSize = ""
				var mimeType = ""
				var resolution = ""
				var commentCount = 0
				var favorites = 0
				var tags: List<String> = emptyList()

				detailBox.select("p").forEach { paragraph ->
					when (paragraph.select("b").text()) {
						"Uploaded" -> {
							uploaded = paragraph.selectFirst("span")!!.attr("title")
						}

						"Views" -> {
							views =
								paragraph.ownText().replace(":", "").replace(",", "").trim().toInt()
						}

						"File size" -> {
							fileSize = paragraph.ownText().replace(":", "")
						}

						"MIME Type" -> {
							mimeType = paragraph.ownText().replace(":", "")
						}

						"Resolution" -> {
							resolution = paragraph.ownText().replace(":", "")
						}

						"Comments" -> {
							commentCount =
								paragraph.ownText().replace(":", "").replace(",", "").trim().toInt()
						}

						"Favorites" -> {
							favorites =
								paragraph.ownText().replace(":", "").replace(",", "").trim().toInt()
						}

						"Tags" -> {
							tags = paragraph.select("span.taglist a").map { tag ->
								tag.text()
							}
						}
					}
				}

				val commentBuilders =
					ArrayList(document.select("div.g-box-contents div.comment").map { comment ->
						createComment(comment)
					})

				commentBuilders.forEachIndexed { index, commentBuilder ->
					if (commentBuilder.depth != 0) {
						// Find the last parent, and append
						commentBuilders.subList(0, index)
							.findLast { it.depth == commentBuilder.depth - 1 }!!.comments.add(
								commentBuilder
							)
					}
				}

				// Prune, as we already have moved sub comments into their parents
				commentBuilders.removeIf { it.depth != 0 }

				val comments = buildCommentBuilders(commentBuilders)


				val itemBox = document.selectFirst("div.item-box")!!

				val iframe = itemBox.selectFirst("iframe")
				if (iframe != null) {

					return@use View.PostView.DocumentView(
						title = title,
						author = authorName,
						authorImageURL = "https:$authorImageURL",
						downloadURL = "https:" + iframe.attr("src"),
						uploaded = uploaded,
						views = views,
						fileSize = fileSize,
						mimeType = mimeType,
						commentCount = commentCount,
						favorites = favorites,
						tags = tags,
						description = description,
						comments = comments,
						id = id,
						next = next,
						previous = prev
					)

				} else {
					val previewURL = itemBox.selectFirst("img")!!.attr("src")
					val downloadURL =
						document.selectFirst("div.func-box div")!!.select("a")[1].attr("href")

					return@use View.PostView.ImagePostView(
						title = title,
						author = authorName,
						authorImageURL = "https:$authorImageURL",
						previewURL = "https:$previewURL",
						downloadURL = "https:$downloadURL",
						uploaded = uploaded,
						views = views,
						fileSize = fileSize,
						mimeType = mimeType,
						commentCount = commentCount,
						favorites = favorites,
						tags = tags,
						description = description,
						comments = comments,
						resolution = resolution,
						id = id,
						next = next,
						previous = prev
					)
				}
			}
		}
	}

	@Throws(HTTPCodeException::class)
	override suspend fun searchGallery(
		text: String?,
		tags: List<String>,
		noTags: Boolean,
		type: SearchType?,
		owner: String?,
		from: LocalDate?,
		to: LocalDate?,
		page: Int,
	): SearchPage {
		val url = g4Search.newBuilder().apply {
			addQueryParameter("q", text)

			addQueryParameter("tags", tags.joinToString(","))

			addQueryParameter("type_search",
				type?.name?.replaceFirstChar { if (it.isLowerCase()) it.titlecase(Locale.ENGLISH) else it.toString() })

			addQueryParameter("user", owner)

			if (noTags) addQueryParameter("notags", "on")

			addQueryParameter(
				"from_date",
				if (from != null) "${from.dayOfMonth}/${from.month}/${from.year}" else null
			)

			addQueryParameter(
				"to_date", if (to != null) "${to.dayOfMonth}/${to.month}/${to.year}" else null
			)


			if (page > 1) addQueryParameter("p", "$page")
		}.build()

		val response = client.newCall(Request.Builder().url(url).build()).executeAsync()
		if (!response.isSuccessful) throw HTTPCodeException(response.code)

		return response.body!!.use { responseBody ->
			val document = Jsoup.parse(responseBody.byteStream(), null, url.toString())
			val pageNav = document.selectFirst("span.pagenav")!!
			val lastPage = document.getLastPage()

			pageNav.selectFirst("span.pagejumps")!!.remove()
			val prevNext = pageNav.select("a")
			val prevURL = if (page != 1) prevNext[0]!!.attr("href") else null
			val nextURL = if (page != lastPage) prevNext[1]!!.attr("href") else null

			pageNav.select("a")
			SearchPage(
				document.getChildViews(), url.toString(), nextURL, prevURL
			)
		}
	}

	@Throws(HTTPCodeException::class)
	override suspend fun getTagCloud(): List<TagCloudItem> {
		val response = client.newCall(Request.Builder().url(g4Tags).build()).executeAsync()
		if (!response.isSuccessful) throw HTTPCodeException(response.code)

		return response.body!!.use { responseBody ->
			val document = Jsoup.parse(responseBody.byteStream(), null, g4Tags.toString())
			val items = document.selectFirst("ul#gallery-items")!!
			items.select("a").map { tag ->
				TagCloudItem(
					tag.text(),
					tag.attr("style").substringAfter(" ").substringBefore("px").toInt()
				)
			}
		}
	}

	@Throws(HTTPCodeException::class)
	override suspend fun searchGalleryTag(tags: List<String>, page: Int): TagsPage {
		val url = g4Tags.newBuilder()
			.addQueryParameter("p", "$page")
			.addQueryParameter("tag", tags.joinToString(","))
			.build()

		val response = client.newCall(Request.Builder().url(url).build()).executeAsync()
		if (!response.isSuccessful) throw HTTPCodeException(response.code)

		return response.body!!.use { responseBody ->
			val document = Jsoup.parse(responseBody.byteStream(), null, url.toString())
			val tagList = document.selectFirst("ul.taglist")!!

			TagsPage(
				document.getChildViews(),
				tagList.select("li").map { li ->
					li.selectFirst("a")!!
				}.map { a ->
					RelatedTag(
						a.text(),
						a.attr("title").toInt()
					)
				},
				page = page,
				maxPage = document.getLastPage()
			)
		}
	}

	@Throws(HTTPCodeException::class)
	override suspend fun getTagQueue(page: Int): TagQueuePage {
		val url = g4TagsQueue.newBuilder()
			.addQueryParameter("p", "$page")
			.build()

		val response = client.newCall(Request.Builder().url(url).build()).executeAsync()
		if (!response.isSuccessful) throw HTTPCodeException(response.code)

		return response.body!!.use { responseBody ->
			val document = Jsoup.parse(responseBody.byteStream(), null, url.toString())
			val lastPage = document.getLastPage()

			TagQueuePage(
				page,
				lastPage,
				document.select("div.g-box-contents")[1]!!.select("div.detail-item").map { div ->
					val itemInfo = div.selectFirst("div.iteminfo")!!
					val thumb = div.selectFirst("a.thumb")!!
					val img = thumb.selectFirst("img")?.attr("src")?.let { "https://$it" }
					val previewText = thumb.selectFirst("p")?.text()

					TagApprovalItem(
						div.attr("id").toInt(),
						title = itemInfo.selectFirst("a")!!.text(),
						imageURL = img,
						previewText = previewText,
						tags = itemInfo.selectFirst("span.taglist")!!.select("a").map { a ->
							a.text()
						},
						proposedTags = itemInfo.selectFirst("ul")!!.select("li a.addtag").map { a ->
							a.text()
						}
					)
				}
			)
		}
	}

	@Throws(HTTPCodeException::class)
	override suspend fun getLatest(page: Int, after: LocalDateTime?): LatestPage {
		val url = g4Latest.newBuilder().apply {
			if (page > 1) {
				after!!
				val year = after.year
				val month = after.month
				val day = after.dayOfMonth
				val hour = after.hour
				val minute = after.minute
				val second = after.second
				addQueryParameter("p", "$page")
				addQueryParameter("after", "$year-$month-$day $hour:$minute:$second")
			}
		}.build()

		val response = client.newCall(Request.Builder().url(url).build()).executeAsync()
		if (!response.isSuccessful) throw HTTPCodeException(response.code)

		return response.body!!.use { responseBody ->
			val document = Jsoup.parse(responseBody.byteStream(), null, url.toString())
			val lastPage = document.getLastPage()

			LatestPage(
				page,
				lastPage,
				document.select("div.g-box-contents div.detail-item").map { div ->
					val thumb = div.selectFirst("a.thumb")!!

					/**
					 * Should contain 5 "p" elements.
					 * 1. Title
					 * 2. Upload date
					 * 3. Owner
					 * 4. Tags
					 * 5. Description
					 */
					val itemInfo = div.selectFirst("div.iteminfo")!!
					val itemInfoItems = itemInfo.select("p")
					val tagList = itemInfo.selectFirst("span.taglist")
					val tags = tagList?.select("a")

					val img = thumb.selectFirst("img")?.attr("src")?.let { "https://$it" }
					val previewText = thumb.selectFirst("p")?.text()

					LatestPageItem(
						id = thumb.attr("href").substringAfterLast("/").toInt(),
						imageURL = img,
						previewText = previewText,
						title = itemInfoItems[0].selectFirst("a")!!.text(),
						time = itemInfoItems[1].selectFirst("span")!!.attr("title"),
						owner = itemInfoItems[2].selectFirst("a")!!.text(),
						tags = tags?.map { it.text() } ?: emptyList(),
						description = itemInfoItems[if (tags != null) 4 else 3].text(),
						comments = div.select("div.comment")
							.map { comment ->
								LatestPageItemComment(
									author = comment.selectFirst("a")!!.text(),
									time = comment.selectFirst("span.pretty-date")!!.attr("title"),
									text = comment.select("p")[1].text()
								)
							}
					)
				}
			)
		}
	}

}