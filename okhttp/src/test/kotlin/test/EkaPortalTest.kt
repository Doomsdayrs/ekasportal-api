/*
 *  Copyright (c) 2023 Doomsdayrs
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package test

import com.gitlab.doomsdayrs.ekasportal.api.OkHttpEkasPortal
import com.gitlab.doomsdayrs.ekasportal.data.SortType
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import okhttp3.OkHttpClient
import kotlin.test.BeforeTest
import kotlin.test.Test

@OptIn(ExperimentalCoroutinesApi::class)
class EkaPortalTest {

	private val okHttpClient = OkHttpClient.Builder().addInterceptor {
		val request = it.request()
		val newRequest = request.newBuilder().addHeader(
			"User-Agent",
			"Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/109.0"
		).build()
		it.proceed(newRequest)
	}.build()

	private lateinit var portal: OkHttpEkasPortal

	@BeforeTest
	fun setup() {
		portal = OkHttpEkasPortal(okHttpClient)
	}

	@Test
	fun galleryBrowse() = runTest {
		val main = portal.getGalleryBrowse(SortType.ALPHABETICAL, 0)
		println(main)
	}

	@Test
	fun galleryViewUser() = runTest {
		// user test
		val view = portal.getGalleryView(621333)
		println(view)
	}

	@Test
	fun galleryViewImage() = runTest {
		// Image Test
		val view = portal.getGalleryView(622764)
		println(view)
	}

	@Test
	fun galleryViewText() = runTest {
		// Text test
		val view = portal.getGalleryView(861776)
		println(view)
	}

	@Test
	fun galleryTagCloud() = runTest {
		val tags = portal.getTagCloud()
		println(tags)
	}

	@Test
	fun galleryTagSearch() = runTest {
		val tagPage = portal.searchGalleryTag(
			listOf("SFW", "Non-Vore"),
			1
		)
		println(tagPage)
	}

	@Test
	fun galleryTagQueue() = runTest {
		val tagQueuePage = portal.getTagQueue(1)
		println(tagQueuePage)
	}

	@Test
	fun galleryLatest() = runTest {
		val latest = portal.getLatest(1, null)
		println(latest)
	}
}