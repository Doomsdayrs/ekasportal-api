val jvm_toolchain: String by rootProject
val coroutines_version: String by rootProject

plugins {
	`java-library`
	kotlin("jvm")
}

java {
	withJavadocJar()
	withSourcesJar()
}

kotlin {
	explicitApi()
	jvmToolchain(jvm_toolchain.toInt())
}

publishing {
	publications {
		create<MavenPublication>("maven") {
			from(components["kotlin"])
		}
	}
}

fun kotlinx(module: String, version: String) =
	"org.jetbrains.kotlinx:$module:$version"

dependencies {
	api(project(":"))
	api("com.squareup.okhttp3:okhttp:4.10.0")
	api("org.jsoup:jsoup:1.15.3")

	testImplementation(kotlin("test"))
	testImplementation(kotlinx("kotlinx-coroutines-test", coroutines_version))
}

tasks {
	test {
		useJUnitPlatform()
	}
}