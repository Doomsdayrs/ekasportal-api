# Eka's Portal Api

This is a faux API for [aryion.com][aryion.com].

## Usage

This API is meant to be cross-platform. But currently it only supports JVM.

If you want, you can create a ktor module for the rest of the platforms.

### Installation

1. Clone
    ```bash
		 git clone https://gitlab.com/Doomsdayrs/ekasportal-api.git
    ```
2. Install (local)
   ```bash
	   cd eksportal-api/
		./gradlew publishToMavenLocal
   ```
3. Add to your project
   ```bash
	repositories {
		mavenLocal()
	}
	dependencies {
		implementation("com.gitlab.doomsdayrs.lib:ekasportal-api-okhttp:0.1.0")
	}
	```

## Support

These are the currently identified features of "aryion.com",
and which are currently accessibly with this API.

If you know better, please help document

- [aryion.com][aryion.com]
	- [x] **g4** Gallery v4
		- [ ] latest.php
			- [x] General
			- [ ] Query
			- List of all updates.
			- Can be paginated,
			  but requires an `after` query with a date value as `YYYY-MM-DD HH:MM:SS`.
			- Can be queried with `name` with with a value of a username.
		- [x] browse.php
			- The page you start off on.
			- Can be paginated.
			- Can be sorted by `mtime` or `title`.
		- [x] tags.php
			- Base url provides some tag highlights.
			- Can be queried for listings.
			- Query can be paginated.
		- [x] tagqueue.php
			- List of tags to be approved.
			- Can be paginated.
		- [x] search.php
			- Search the gallery.
			- Can be paginated.
			- Can be queried with a variety of tags
		- [x] /view/####
			- Display the content of a view, should be sub-linked with the view id.
		- [ ] settings.php
			- May require `id` with value of a username,
			  doesn't need to if user is already logged in.
			- Can be queried with `edit`, possible values listed
				- profile
				- tags
				- zebra (friends & foes)
				- theme
				- commission
				- submission
				- avatar
		- [ ] /user/####
			- Replace #### with username
		- [ ] /favorites/####
			- Replace #### with username
		- [ ] userpage.commission.php
			- Must be provided `id` with value of a username.
		- [ ] /blog/####
			- Replace #### with username
- [ ] **forum** Forum endpoint

[aryion.com]: https://aryion.com