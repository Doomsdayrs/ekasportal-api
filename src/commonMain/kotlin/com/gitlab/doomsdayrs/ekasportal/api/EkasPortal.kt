/*
 *  Copyright (c) 2023 Doomsdayrs
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gitlab.doomsdayrs.ekasportal.api

import com.gitlab.doomsdayrs.ekasportal.data.*
import kotlinx.datetime.LocalDate
import kotlinx.datetime.LocalDateTime

/**
 * API Interface for Eka's Portal.
 */
public interface EkasPortal {

	// g4

	/**
	 * Load a specific view from the gallery
	 *
	 * @param id view identification
	 *
	 * @see <a href="https://aryion.com/g4/view/">g4/view</a>
	 */
	public suspend fun getGalleryView(id: Int): View

	/**
	 * Load the gallery overview
	 *
	 * @param sortType How the gallery is sorted
	 * @param page The page to load from the
	 *
	 * @see <a href="https://aryion.com/g4/browse.php">g4/browse.php</a>
	 */
	public suspend fun getGalleryBrowse(
		sortType: SortType,
		page: Int
	): BrowsePage

	/**
	 * Search the gallery
	 *
	 * @param text text to query
	 * @param tags tags to include
	 * @param noTags TODO what is this?
	 * @param type What type of media to look for
	 * @param owner Which author to check
	 * @param from Starting date for posts
	 * @param to Ending date for posts
	 * @param page What page to load from, should always start at 0
	 *
	 * @see <a href="https://aryion.com/g4/search.php">g4/search.php</a>
	 */
	public suspend fun searchGallery(
		text: String?,
		tags: List<String>,
		noTags: Boolean,
		type: SearchType?,
		owner: String?,
		from: LocalDate?,
		to: LocalDate?,
		page: Int
	): SearchPage

	/**
	 * Get the main page from "g4/tags.php".
	 *
	 * @see <a href="https://aryion.com/g4/tags.php">g4/tags.php</a>
	 */
	public suspend fun getTagCloud(): List<TagCloudItem>

	/**
	 * Query "g4/tags.php".
	 *
	 * @param tags Tags to search
	 * @param page Page to load
	 *
	 * @see <a href="https://aryion.com/g4/tags.php">g4/tags.php</a>
	 */
	public suspend fun searchGalleryTag(tags: List<String>, page: Int): TagsPage

	/**
	 * Get the queue of tags to be approved.
	 *
	 * @param page Page to load
	 *
	 * @see <a href="https://aryion.com/g4/tagqueue.php">g4/tagqueue.php</a>
	 */
	public suspend fun getTagQueue(page: Int): TagQueuePage

	/**
	 * Get the latest posts.
	 *
	 * @param page page to load
	 * @param after if [page] is 2, must be the date value from the last pages item.
	 *
	 * @see <a href="https://aryion.com/g4/latest.php">g4/latest.php</a>
	 */
	public suspend fun getLatest(page: Int, after: LocalDateTime?): LatestPage
}