/*
 *  Copyright (c) 2023 Doomsdayrs
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gitlab.doomsdayrs.ekasportal.data

/**
 * Represents any view on the aryion gallery
 */
public sealed class View {
	public abstract val id: Int
	public abstract val url: String
	public abstract val title: String

	/**
	 * Next page, if exists
	 */
	public abstract val next: String?

	/**
	 * Previous page, if exists
	 */
	public abstract val previous: String?


	/**
	 * Represents a folder
	 */
	public data class FolderView(
		override val id: Int,
		override val title: String,

		val page: Int,
		val maxPage: Int,
		val views: List<SimpleItem>,
	) : View() {
		override val url: String = createViewURL(id, page)

		override val next: String? = if (page < maxPage) createViewURL(id, page + 1) else null
		override val previous: String? = if (page == 1) null else createViewURL(id, page - 1)
	}

	/**
	 * Represents any post
	 */
	public sealed class PostView : View() {
		override val url: String by lazy { createViewURL(id) }

		public abstract val author: String
		public abstract val authorImageURL: String

		public abstract val downloadURL: String

		public abstract val uploaded: String
		public abstract val views: Int
		public abstract val fileSize: String
		public abstract val mimeType: String
		public abstract val commentCount: Int
		public abstract val favorites: Int
		public abstract val tags: List<String>

		public abstract val description: String

		public abstract val comments: List<Comment>

		/**
		 * Represents an image post
		 */
		public data class ImagePostView(
			override val title: String,
			override val author: String,
			override val authorImageURL: String,

			val previewURL: String,
			override val downloadURL: String,

			override val uploaded: String,
			override val views: Int,
			override val fileSize: String,
			override val mimeType: String,
			override val commentCount: Int,
			override val favorites: Int,
			override val tags: List<String>,
			override val description: String,
			override val comments: List<Comment>,

			val resolution: String,
			override val id: Int,
			override val next: String?,
			override val previous: String?,
		) : PostView()

		/**
		 * Represents a document that must be opened externally
		 */
		public data class DocumentView(
			override val title: String,
			override val author: String,
			override val authorImageURL: String,

			override val downloadURL: String,

			override val uploaded: String,
			override val views: Int,
			override val fileSize: String,
			override val mimeType: String,
			override val commentCount: Int,
			override val favorites: Int,
			override val tags: List<String>,
			override val description: String,
			override val comments: List<Comment>,
			override val id: Int,
			override val next: String?,
			override val previous: String?
		) : PostView()
	}

	public companion object {
		public fun createViewURL(id: Int): String =
			"$id"

		public fun createViewURL(id: Int, page: Int): String =
			createViewURL(id) + "?p=$page"
	}
}