/*
 *  Copyright (c) 2023 Doomsdayrs
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gitlab.doomsdayrs.ekasportal.data

/**
 * Item in [LatestPage]
 *
 * @see [LatestPage]
 */
public data class LatestPageItem(
	val id: Int,
	val imageURL: String?,
	val previewText: String?,
	val title: String,
	val time: String,
	val owner: String,
	val tags: List<String>,
	val description: String,
	val comments: List<LatestPageItemComment>
)

/**
 * Comments found sometimes on [LatestPageItem]
 *
 * @see [LatestPageItem]
 * @see [LatestPage]
 */
public data class LatestPageItemComment(
	val author: String,
	val time: String,
	val text: String
)

/**
 * Represents result from latest.php
 *
 * @see <a href="https://aryion.com/g4/latest.php">g4/latest.php</a>
 */
public data class LatestPage(
	val page: Int,
	val maxPage: Int,
	val items: List<LatestPageItem>
)