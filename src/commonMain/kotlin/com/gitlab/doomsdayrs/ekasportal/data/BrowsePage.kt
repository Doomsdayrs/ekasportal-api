/*
 *  Copyright (c) 2023 Doomsdayrs
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gitlab.doomsdayrs.ekasportal.data


/**
 * Represents the main archive page
 *
 * @see <a href="https://aryion.com/g4/browse.php">g4/browse.php</a>
 */
public data class BrowsePage(
	val sortType: SortType,
	val page: Int,
	val maxPage: Int,
	val views: List<SimpleItem>,
)