/*
 *  Copyright (c) 2023 Doomsdayrs
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gitlab.doomsdayrs.ekasportal.data

/**
 * Tags related to the current query in [TagsPage]
 *
 * @param tag Tag that is somewhat related
 * @param count How many entries match this tag.
 *
 * @see [TagsPage]
 */
public data class RelatedTag(
	val tag: String,
	val count: Int
)

/**
 * Represents a page from "tags.php" query
 *
 * @see <a href="https://aryion.com/g4/tags.php">g4/tags.php</a>
 */
public data class TagsPage(
	val items: List<SimpleItem>,
	val related: List<RelatedTag>,
	val page: Int,
	val maxPage: Int
)

/**
 * Represents tag from "tags.php" root
 *
 * @see <a href="https://aryion.com/g4/tags.php">g4/tags.php</a>
 */
public data class TagCloudItem(
	val tag: String,
	val size: Int
)